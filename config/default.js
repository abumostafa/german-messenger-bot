module.exports = {
    server: {
        port: 8882,
        host: 'https://german-messenger-bot.veskal.com/v1'
    },
    facebook: {
        app: {
            verify_token: '539sCP27Y8uqsXeXERNfPPuQRAR6apNR2yTudKbNvf3xQ7kkzJre9vFQF9yt8bUYzDhpY52jbEtRnpMMXNCW8TqrSMHQNtEXGyXw'
        },
        page: {
            token: 'EAAZAvzOhYzxcBAGDzHkotXvEZBT1lRLrO0ROx1O4QvWKjHjtkvgp47CUEM2V7fu5Of9SL6UVBeCiZAyyKyo4n8lROXE8p3yaXrlMo2KpnlRsapLZCBb52HOVclgZBHZBoIvyZCbDDy4ZCjgyGHhUNU1JP3cX5ZAir5sj3KDKViX5mZCgZDZD'
        },
        api: {
            host: 'https://graph.facebook.com/v2.6',
        }
    },
    pons: {
        api:{
            host: "https://api.pons.com/v1",
            secret: "b848f2c31f486865ec87b0474237a73f7808f9494548b2ee5cc84d2fe2303ab2",
        }
    },
    db: {
        connection_url: `mongodb://localhost:27017/german_messenger_bot`
    }
};